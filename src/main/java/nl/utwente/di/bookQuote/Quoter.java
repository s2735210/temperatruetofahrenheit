package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {
    private final Map<String, Double> m = new HashMap<String, Double>() {{
        put("1", 10.0);
        put("2", 45.0);
        put("3", 20.0);
        put("4", 35.0);
        put("5", 50.0);
    }};
    public double getFarenheit(String isbn) {
        if (isbn.matches("-?(0|[1-9]\\d*)")) {
            int isbnInt = Integer.parseInt(isbn);
            return (isbnInt * 1.8) + 32;
        } else {
            return -999;
        }
    }
}
